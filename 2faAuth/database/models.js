const rdb = require('rethinkdb')

module.exports = (table) => {
  return {
    getAll: async (conn) => {
      return new Promise((resolve) => {
        rdb.table(table).run(conn, (_, results) => resolve(results.toArray()))
      })
    },

    get: async (conn, filter) => {

      return new Promise((resolve) => {
        rdb
          .table(table)
          .filter(filter)
          .run(conn, (_, result) => resolve(result.toArray()))
      })
    },

    create: (conn, data, callback = null) => {
      rdb.table(table).insert(data).run(conn, callback)
    },

    update: (conn, id, data, callback = null) => {
      rdb.table(table).get(id).update(data).run(conn, callback)
    },
  }
}
