const rdb = require('rethinkdb')
const config = require('../config')
const tables = require('./tables')

module.exports = async () => {
  console.log('Setting up RethingDB...')

  const conn = await new Promise((resolve) => {
    rdb
      .connect({
        host: config.DATABASE.HOST,
        port: config.DATABASE.PORT,
        db: config.DATABASE.NAME,
        user: config.DATABASE.USER,
        password: config.DATABASE.PASSWORD,
      })
      .then((conn) => {
        resolve(conn)
      })
      .error((error) => {
        throw error
      })
  })

  // Check Database
  await rdb
    .dbCreate(config.DATABASE.NAME)
    .run(conn)
    .then(() => console.log('Database created...'))
    .error(() => console.log('Database already created...'))

  // Check Tables
  tables.forEach((table) => {
    rdb
      .tableCreate(table)
      .run(conn)
      .then(() => console.log(`Table ${table} created...`))
      .error((e) => console.log(`Table ${table} already created...`))
  })

  return conn
}
