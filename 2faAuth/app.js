const express = require('express')
const http = require('http')
const bcrypt = require('bcrypt')
const speakeasy = require('speakeasy')
const qrCode = require('qrcode')

const config = require('./config')
const databaseSetup = require('./database/setup')
const userModel = require('./database/models')('Users')

const app = express()
const server = http.createServer(app)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.post('/api/register', async (req, res) => {
  try {
    const conn = req.app.locals.conn

    const { name, email, password } = req.body
    const users = await userModel.get(conn, { email })

    // console.log(req.body);

    if (users.length > 0) {
      res.json({ error: 'User already exist!' })
      return;
    }

    const hashedPassword = await bcrypt.hash(password, 10)
  //  const secret = speakeasy.generateSecret()

    const user = userModel.create(conn, {
      name,
      email,
      password: hashedPassword,
    //  secret: secret.base32,
    })
    res.json({ user })
/*     const qrCodeUrl = await qrCode.toDataURL(secret.otpauth_url)

    res.json({
      qrCodeUrl,
      secret: secret.base32,
    }) */
  } catch (error) {
    // console.log( error.message )
    res.json({ error: error.message })
  }
})

app.post('/api/login', async (req, res) => {
  const conn = req.app.locals.conn
  const { email, password } = req.body

  const user = (await userModel.get(conn, { email }).catch(() => [])).pop()

  if (
    !user ||
    !bcrypt.compareSync(password, user.password) 
  /*   !speakeasy.totp.verify({
      secret: user.secret,
      encoding: 'base32',
      token,
    }) */
  ) {
    return res.json({ error: 'Invalid credentials' })
  }

  res.json({ user })
})

server.listen(config.APP_PORT, async () => {
  console.log('Listening on port %d', config.APP_PORT)
  app.locals.conn = await databaseSetup()
})
