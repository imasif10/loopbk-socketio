import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {RethinkdbDataSource} from '../datasources';
import {tracks} from '../models';

export class TracksRepository extends DefaultCrudRepository<
    tracks,
  typeof tracks.prototype.id
> {
  constructor(
    @inject('datasources.rethinkdb') dataSource: RethinkdbDataSource,
  ) {
    super(tracks, dataSource);
  }
}
