import {inject} from "@loopback/core";
import {Request, Response} from "@loopback/express";
import {
  repository
} from '@loopback/repository';
import {
  get, param, post, requestBody,
  RestBindings,
  SchemaObject
} from '@loopback/rest';
import {tracks} from '../models';
import {TracksRepository} from '../repositories';
import {TracksService} from "../services";

const TracksParametersSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'token'],
  properties: {
    playlist: {
      type: 'object',
    },
    userId: {
      type: 'string',
    },
  },
};

export class TracksController {
  constructor(
    @repository(TracksRepository)
    public tracksRepository: TracksRepository,

    @inject('services.TracksService')
    public tracksService: TracksService,

  ) { }

  @post('/tracks', {
    responses: {
      '200': {
        description: 'Tracks model instance',
        content: {
          'application/json': {schema: TracksParametersSchema},
        },
      },
    },
  })
  async create(
    @inject(RestBindings.Http.REQUEST) req: Request,
    @inject(RestBindings.Http.RESPONSE) res: Response,
    @requestBody() requestBody,
  ): Promise<tracks> {
    try {
      const pause = req.app.get('pause')
      const skip = []
      const interval = []
      const io = req.app.get('locals.io')

      const {playlist, userId} = req.body
      const uniqueKey = `${playlist.uid}_${userId}`


      // Clear Interval and Initialize global variables
      clearInterval(interval[uniqueKey])
      pause[uniqueKey] = false
      skip[uniqueKey] = false

      // Get current Task data in DB, create if not exists
      const currentTrack = await this.tracksRepository.findOne({
        where: {
          playlist_uid: playlist.uid,
          user_id: userId,
        }
      })

      // console.log(io, 'io')
      // console.log(req.app.get('locals.io'), 'locals.io')
      // console.log(req.app.get('locals.site_url'), 'locals.site_url')
      // console.log(currentTrack, 'currentTrack')
      let trackId = currentTrack ? currentTrack.id : null

      if (!trackId) {
        // console.log(playlist, 'playlist')

        const tracksParam = {
          playlist_uid: playlist.uid,
          user_id: userId
        } as tracks

        const trackResponse = await this.tracksRepository.create(tracksParam)

        trackId = trackResponse.id;
      }


      let index = 0
      let track = null

      // console.log(pause, 'pause')

      while ((track = playlist.tracks[index])) {
        let seconds = 0

        // Update current track in DB
        await this.tracksRepository.updateAll({
          ...track,
          position: 0,
          timestamp: new Date(Date.now()).toDateString(),
          location: playlist.name,
        }, {
          id: trackId
        })

        await new Promise<void>((resolve) => {
          // Set 1 second per interval
          interval[uniqueKey] = setInterval(() => {

            // Return if pause for specific playlist is set to true
            // console.log(pause, 'pause')
            if (pause[uniqueKey] === true) {
              return
            }

            // Check counter every +10 seconds and update track data in db
            if (
              seconds % 10 === 0 &&
              seconds <= track.duration &&
              skip[uniqueKey] === false
            ) {

              this.tracksRepository.updateAll({
                position: seconds,
              }, {
                id: trackId
              })
            }

            // Check if counter exceed track max duration or playlist is skip
            if (seconds > track.duration || skip[uniqueKey] === true) {
              skip[uniqueKey] = false
              seconds = 0
              clearInterval(interval[uniqueKey])
              resolve()
            }

            // IO Emit to be used by player app

            io.emit('counter', {
              uid: playlist.uid,
              currentTrack: track.title,
              counter: seconds,
            })


            seconds++
          }, 1000)
        })

      }

      return {
        id: trackId
      } as tracks;

    } catch (e) {
      return Promise.reject(e);
    }
  }


  @get('tracks/{id}/pause', {
    responses: {
      '200': {
        description: 'Tracks model instance',
        content: {
          'application/json': {
          }
        },
      },
    },
  })
  async findById(
    @inject(RestBindings.Http.REQUEST) req: Request,
    @inject(RestBindings.Http.RESPONSE) res: Response,
    @param.path.string('id') id: string,
    @requestBody() requestBody
  ): Promise<{pause: string}> {

    try {
      const pause = req.app.get('pause')

      // @ts-ignore
      const uniqueKey = `${id}_${req.query.uid}`

      pause[uniqueKey] = !pause[uniqueKey]


      return {
        pause: pause[uniqueKey],
      };

    } catch (e) {
      return Promise.reject(e);
    }

  }



  // @get('/tracks/count')
  // @response(200, {
  //   description: 'Tracks model count',
  //   content: {'application/json': {schema: CountSchema}},
  // })
  // async count(
  //     @param.where(Tracks) where?: Where<Tracks>,
  // ): Promise<Count> {
  //   return this.tracksRepository.count(where);
  // }
  //
  // @get('/tracks')
  // @response(200, {
  //   description: 'Array of Tracks model instances',
  //   content: {
  //     'application/json': {
  //       schema: {
  //         type: 'array',
  //         items: getModelSchemaRef(Tracks, {includeRelations: true}),
  //       },
  //     },
  //   },
  // })
  // async find(
  //     @param.filter(Tracks) filter?: Filter<Tracks>,
  // ): Promise<Tracks[]> {
  //   return this.tracksRepository.find(filter);
  // }
  //
  // @patch('/tracks')
  // @response(200, {
  //   description: 'Tracks PATCH success count',
  //   content: {'application/json': {schema: CountSchema}},
  // })
  // async updateAll(
  //     @requestBody({
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(Tracks, {partial: true}),
  //         },
  //       },
  //     })
  //         tracks: Tracks,
  //     @param.where(Tracks) where?: Where<Tracks>,
  // ): Promise<Count> {
  //   return this.tracksRepository.updateAll(tracks, where);
  // }
  // @patch('/tracks/{id}')
  // @response(204, {
  //   description: 'Tracks PATCH success',
  // })
  // async updateById(
  //   @param.path.number('id') id: string,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Tracks, {partial: true}),
  //       },
  //     },
  //   })
  //   tracks: Tracks,
  // ): Promise<void> {
  //   await this.tracksRepository.updateById(id, tracks);
  // }
  //
  // @put('/tracks/{id}')
  // @response(204, {
  //   description: 'Tracks PUT success',
  // })
  // async replaceById(
  //   @param.path.number('id') id: string,
  //   @requestBody() tracks: Tracks,
  // ): Promise<void> {
  //   await this.tracksRepository.replaceById(id, tracks);
  // }
  //
  // @del('/tracks/{id}')
  // @response(204, {
  //   description: 'Tracks DELETE success',
  // })
  // async deleteById(@param.path.number('id') id: string): Promise<void> {
  //   await this.tracksRepository.deleteById(id);
  // }
}
