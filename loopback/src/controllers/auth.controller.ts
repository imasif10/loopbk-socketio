// Uncomment these imports to begin using these cool features!
import {repository} from '@loopback/repository';
import {post, getModelSchemaRef, requestBody, response} from '@loopback/rest';
import bcrypt from 'bcrypt';
import qrCode from 'qrcode';
import speakeasy from 'speakeasy';
import {Users} from '../models';
import {Login, UserLogin} from '../models';
import {UsersRepository} from '../repositories';

export class AuthController {
  constructor(
    @repository(UsersRepository)
    public usersRepository: UsersRepository,
  ) {}

  @post('/authenticate/register')
  @response(200, {
    description: 'Register',
    content: {'application/json': {schema: getModelSchemaRef(Users)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Users, {
            title: 'register',
            exclude: ['id', 'secret'],
          }),
        },
      },
    })
    users: Users,
  ) {
    try {
      let user = await this.usersRepository.findOne({
        where: {
          email: users.email,
        },
      });
      if (user) {
        return 'User already exist!';
      }
      const hashedPassword = await bcrypt.hash(users.password, 10);
      const secret = speakeasy.generateSecret();
      await this.usersRepository.create({
        ...users,
        password: hashedPassword,
        secret: secret.base32,
      });
      const qrCodeUrl = await qrCode.toDataURL(secret.otpauth_url as string);
      return {
        qrCodeUrl,
        secret: secret.base32,
      };
    } catch (ex) {
      return {
        error: ex.message,
      };
    }
  }

  @post('/authenticate/verify')
  @response(200, {
    description: 'Login',
    content: {'application/json': {schema: getModelSchemaRef(Login)}},
  })
  async login(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Login, {
            title: 'login',
            exclude: ['id'],
          }),
        },
      },
    })
    login_user: Login,
  ) {
    try {
      const {email, password, token} = login_user;
      let user = await this.usersRepository.findOne({
        where: {
          email,
        },
      });
      console.log(1, user, email, password, token);
      console.log(2, !bcrypt.compareSync(password, user ? user.password : ''));
      console.log(
        !speakeasy.totp.verify({
          secret: user ? user.secret : '',
          encoding: 'base32',
          token,
        }),
      );

      if (
        !user ||
        !bcrypt.compareSync(password, user.password) ||
        !speakeasy.totp.verify({
          secret: user.secret,
          encoding: 'base32',
          token,
        })
      ) {
        return {
          error: 'Invalid credentials',
        };
      }
      return {
        user,
      };
    } catch (ex) {
      return {
        error: ex.message,
      };
    }
  }

  @post('/authenticate/login')
  @response(200, {
    description: 'User Login',
    content: {'application/json': {schema: getModelSchemaRef(UserLogin)}},
  })
  async user_login(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserLogin, {
            title: 'user login',
            exclude: ['id'],
          }),
        },
      },
    })
    login_user: UserLogin,
  ) {
    try {
      const {email, password} = login_user;
      let user = await this.usersRepository.findOne({
        where: {
          email,
        },
      });

      if (!user || !bcrypt.compareSync(password, user.password)) {
        return {
          error: 'Invalid credentials',
        };
      }
      return {
        user,
      };
    } catch (ex) {
      return {
        error: ex.message,
      };
    }
  }
}
