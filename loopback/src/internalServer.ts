import express from "express";
require('dotenv').config()

export async function startInternalServer(): Promise<(resolve, reject) => unknown> {

  const app = express();
  const internalServer = require("http").createServer(app);
  
  return new Promise((resolve, reject): void => {
    const port = process.env.INTERNAL_SERVER_PORT;
    internalServer.listen(port, null, (err) => {
      if (err) {
        return reject(err);
      }
      resolve(internalServer);
      console.log("Loopback Application listening on " + port);
    });
  });
}
