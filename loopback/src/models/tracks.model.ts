import {Entity, model, property} from '@loopback/repository';

@model()
export class tracks extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;
  
  @property({
    type: 'string',
    required: true,
  })
  user_id?: string;
  
  @property({
    type: 'string',
  })
  playlist_uid?: string;
  
  @property({
    type: 'number',
  })
  position?: number;
  
  @property({
    type: 'number',
  })
  duration?: number;
  
  @property({
    type: 'string',
  })
  location?: string;
  
  @property({
    type: 'string',
  })
  timestamp?: Date;
  
  @property({
    type: 'string',
  })
  title?: string;
  
  constructor(data?: Partial<tracks>) {
    super(data);
  }
}

