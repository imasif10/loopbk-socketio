import {ApplicationConfig} from './application';
import {ExpressServer} from "./server";
//import {startInternalServer} from "./internalServer";
require('dotenv').config()
export * from './application';

export async function main(options: ApplicationConfig = {}) {
  const server = new ExpressServer(options);
  await server.boot();
  await server.startServer(process.env.SERVER_HOST, Number(process.env.SERVER_PORT));
  //await startInternalServer();
}
(async (): Promise<boolean> => {
if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      port: process.env.SERVER_PORT,
      host: process.env.SERVER_HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
      // Use the LB4 application as a route. It should not be listening.
      listenOnStart: false,
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
  return true;
})().finally(() => true);
