import {once} from "events";
import express from 'express';
import * as http from "http";
import * as path from "path";
import {ApplicationConfig, LoopbackApplication} from './application';

require('dotenv').config();
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
const expressEjsExtend = require('express-ejs-extend')

// const socketIO = require('socket.io')


export {ApplicationConfig};
export class ExpressServer {
  public readonly app: express.Application;
  public readonly lbApp: LoopbackApplication;
  private server?: http.Server;

  constructor(options: ApplicationConfig = {}) {

    this.app = express();
    this.lbApp = new LoopbackApplication(options);
    // @ts-ignore

    // Expose the front-end assets via Express, not as LB4 route
    this.app.use(`/`, this.lbApp.requestHandler);
    this.app.use(express.json())
    this.app.use(express.urlencoded({extended: true}))
    this.app.use(express.static(path.join(__dirname, 'public')))
    this.app.set('views', `${__dirname}/views`)

    this.app.engine('ejs', expressEjsExtend)
    this.app.set('view engine', 'ejs')
    this.app.use(flash())
    this.app.use(
      session({
        secret: '12345',
        resave: false,
        saveUninitialized: false,
      })
    )

    passport.serializeUser((user, done) => done(null, user.id))
    passport.deserializeUser((id, done) => done(null, id))
    this.app.use(passport.initialize())
    this.app.use(passport.session())
  }



  async boot() {
    await this.lbApp.boot();
  }


  public async startServer(serverHost: string, serverPort: number) {
    await this.lbApp.start();

    const ioServer = require("http").createServer(this.app);
    const io = require("socket.io")(ioServer, {
      cors: {
        origin: '*',
      }
    });


    // Global Variables
    this.app.set('locals.io', io)
    this.app.set('pause', [])
    this.app.set('locals.site_url', `http://localhost:${process.env.SERVER_PORT}`)

    io.of("").on("connection", (socket: any) => {
      socket.emit("init", "connected ")
    });
    this.server = ioServer.listen(serverPort, serverHost, () => {
      console.log("Express server listening on " + serverPort);
    });

    await once(this.app, 'this.app');
  }


  // For testing purposes
  public async stop() {
    if (!this.server) return;
    await this.lbApp.stop();
    this.server.close();
    await once(this.server, "close");
    this.server = undefined;
  }
}
