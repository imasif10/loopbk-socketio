import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import {repository} from "@loopback/repository";
import {TracksRepository} from "../repositories";

@injectable({scope: BindingScope.TRANSIENT})
export class TracksService {
  constructor(
      @repository(TracksRepository)
      public tracksRepository : TracksRepository,
  ) {}

  /*
   * Add service methods here
   */
  public async setInterval(io, track, playlist, options) {
    let seconds = 0
    return setInterval(() => {
      // Return if pause for speficic playlist is set to true
      if (options.pause[options.uniqueKey] === true) return
    
      // Check counter every +10 seconds and update track data in db
      if (
          seconds % 10 === 0 &&
          seconds <= track.duration &&
          options.skip[options.uniqueKey] === false
      ) {
      
        this.tracksRepository.updateById(options.trackId, {
          position: seconds,
        })
      }
    
      // Check if counter exceed track max duration or playlist is skip
      if (seconds > track.duration || options.skip[options.uniqueKey] === true) {
        options.skip[options.uniqueKey] = false
        options.index++
        seconds = 0
        clearInterval(options.interval[options.uniqueKey])
      }
    
      // IO Emit to be used by player app
      io.emit('counter', {
        uid: playlist.uid,
        currentTrack: track.title,
        counter: seconds,
      })
    
      seconds++
    }, 1000)
  }
}
