const express = require('express')
const path = require('path')
const http = require('http')
const fetch = require('node-fetch')
const socketIO = require('socket.io')
const socketIOClient = require('socket.io-client')

const config = require('./config')
const playlist = require('./playlist')
const databaseSetup = require('./database/setup')
const userModel = require('./database/models')('Users')

const app = express()
const server = http.createServer(app)
const io = socketIO(server)
const client = socketIOClient.connect(config.CLIENT_URL)

// Use to connect Socket IO to different port/url
client.once('connect', () => {
  console.log('Socket Client connected to %s', config.CLIENT_URL)

  client.on('counter', (counter) => {
    io.emit('counter', counter)
  })
})

// Middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', `${__dirname}/views`)
app.set('view engine', 'ejs')

// Global Variables
app.locals.SITE_URL = `http://localhost:${config.APP_PORT}`

// Routes
app.get('/', async (req, res) => {
  const users = await userModel.getAll(req.app.locals.conn)

  res.render('index', { playlist, users })
})

app.post('/play/:uid', (req, res) => {
  const uid = req.params.uid
  const userId = req.body.userId

  fetch(`${config.CLIENT_URL}/tracks`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      playlist: playlist.find((pl) => pl.uid === uid),
      userId,
    }),
  })

  res.sendStatus(200)
})

server.listen(config.APP_PORT, async () => {
  console.log('Listening on port %d', config.APP_PORT)
  app.locals.conn = await databaseSetup({ io })
})
