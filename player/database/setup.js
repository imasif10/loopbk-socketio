const rdb = require('rethinkdb')
const config = require('../config')
const tables = require('./tables')

module.exports = async ({ io }) => {
  console.log('Setting up RethingDB...')

  const conn = await new Promise((resolve) => {
    rdb
      .connect({
        host: config.DATABASE.HOST,
        port: config.DATABASE.PORT,
        db: config.DATABASE.NAME,
        user: config.DATABASE.USER,
        password: config.DATABASE.PASSWORD,
      })
      .then((conn) => {
        resolve(conn)
      })
      .error((error) => {
        throw error
      })
  })

  // Check Database
  await rdb
    .dbCreate(config.DATABASE.NAME)
    .run(conn)
    .then(() => console.log('Database created...'))
    .error(() => console.log('Database already created...'))

  // Check Tables
  tables.forEach(async (table) => {
    await rdb
      .tableCreate(table)
      .run(conn)
      .then(() => console.log(`Table ${table} created...`))
      .error(() => console.log(`Table ${table} already created...`))

    // Setup Listener
    if (table === 'tracks') {
      rdb
        .table(table)
        .changes()
        .run(conn)
        .then((cursor) => {
          cursor.each((_, row) => {
            if (row.new_val) {
              io.emit(
                row.old_val === null ? 'trackCreated' : 'trackUpdated',
                row.new_val
              )
            }
          })
        })
    }
  })

  return conn
}
