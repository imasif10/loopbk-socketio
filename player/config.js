require('dotenv').config()

module.exports = {
  APP_PORT: process.env.APP_PORT || 3000,
  DATABASE: {
    NAME: process.env.DATABASE_NAME,
    HOST: process.env.DATABASE_HOST,
    PORT: process.env.DATABASE_PORT,
  },
  CLIENT_URL: process.env.CLIENT_URL,
}
