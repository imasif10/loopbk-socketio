module.exports = [
  {
    uid: 'af20fda7-0734-40a7-9147-07e07c9a79f4',
    name: 'Playlist 1',
    tracks: [
      {
        title: 'track 1',
        duration: 150,
      },
      {
        title: 'track 2',
        duration: 140,
      },
      {
        title: 'track 3',
        duration: 40,
      },
      {
        title: 'track 4',
        duration: 75,
      },
      {
        title: 'track 5',
        duration: 150,
      },
      {
        title: 'track 6',
        duration: 140,
      },
      {
        title: 'track 7',
        duration: 240,
      },
      {
        title: 'track 8',
        duration: 175,
      },
    ],
  },
  {
    uid: 'af20fda7-0734-40a7-9147-07e07c9a79f5',
    name: 'Playlist 2',
    tracks: [
      {
        title: 'track 100',
        duration: 60,
      },
      {
        title: 'track 101',
        duration: 70,
      },
    ],
  },
]
