require('dotenv').config()

module.exports = {
  APP_PORT: process.env.PORT || 3006,
  LOOPBACK_PORT: process.env.LOOPBACK_PORT || 3010,
  DATABASE: {
    NAME: process.env.RETHINKDB_NAME,
    HOST: process.env.RETHINKDB_HOST,
    PORT: process.env.RETHINKDB_PORT,
    USER: process.env.RETHINKDB_USERNAME,
    PASSWORD: process.env.RETHINKDB_PASSWORD,
  },
  TWOFA_URL: process.env.TWOFA_URL,
}
