const express = require('express')
const path = require('path')
const http = require('http')
const socketIO = require('socket.io')
const expressEjsExtend = require('express-ejs-extend')
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')

const config = require('./config')
const routes = require('./routes')
const databaseSetup = require('./database/setup')

const app = express()
const server = http.createServer(app)
const io = socketIO(server)

// Middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', `${__dirname}/views`)

app.engine('ejs', expressEjsExtend)
app.set('view engine', 'ejs')

app.use(flash())
app.use(
  session({
    secret: '12345',
    resave: false,
    saveUninitialized: false,
  })
)

passport.serializeUser((user, done) => done(null, user.id))
passport.deserializeUser((id, done) => done(null, id))
app.use(passport.initialize())
app.use(passport.session())

// Global Variables
app.locals.SITE_URL = `http://localhost:${config.APP_PORT}`
app.locals.LOOPBACK_URL = `http://localhost:${config.LOOPBACK_PORT}`
app.locals.io = io

routes(app)

server.listen(config.APP_PORT, async () => {
  console.log('Listening on port %d', config.APP_PORT)
  app.locals.conn = await databaseSetup({ io })
})
