const trackModel = require('../database/models')('tracks')

module.exports = {
  index: async (req, res) => {
    const conn = req.app.locals.conn

    console.log("req.user",req.user);

    const tracks = await trackModel
      .get(conn, { user_id: req.user })
      .catch(() => [])

    console.log("tracks", tracks);


    res.render('pages/home', { tracks })
  },

  login: (_, res) => {
    res.render('login')
  },
}
