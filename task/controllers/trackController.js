const trackModel = require('../database/models')('tracks')

let pause = []
let skip = []
let interval = []

module.exports = {
  view: async (req, res) => {
    const conn = req.app.locals.conn
    const trackId = req.params.trackId
    const track = (
      await trackModel
        .get(conn, { id: trackId, user_id: req.user })
        .catch(() => [])
    ).pop()

    if (!track) {
      return res.redirect('/')
    }

    res.render('pages/tracks/view.track.ejs', {
      id: track.id,
      location: track.location,
      title: track.title,
      position: track.position,
      duration: track.duration,
      playlist_uid: track.playlist_uid,
      user_id: req.user
    })
  },

  create: async (req, res) => {
    const conn = req.app.locals.conn
    const io = req.app.locals.io

    const { playlist, userId } = req.body
    const uniquekey = `${playlist.uid}_${userId}`

    // Clear Interval and Initialize global variables
    clearInterval(interval[uniquekey])
    pause[uniquekey] = false
    skip[uniquekey] = false

    // Get current Task data in DB, create if not exists
    const currentTrack = (
      await trackModel.get(conn, {
        playlist_uid: playlist.uid,
        user_id: userId,
      })
    ).pop()
    let trackId = currentTrack ? currentTrack.id : null

    if (!trackId) {
      trackId = await new Promise((resolve) => {
        trackModel.create(
          conn,
          { playlist_uid: playlist.uid, user_id: userId },
          (_, result) => {
            resolve(result.generated_keys[0])
          }
        )
      })
    }

    let index = 0
    let track = null

    while ((track = playlist.tracks[index])) {
      let seconds = 0

      // Update current track in DB
      trackModel.update(conn, trackId, {
        ...track,
        position: 0,
        timestamp: new Date(Date.now()).toDateString(),
        location: playlist.name,
      })

      await new Promise((resolve) => {
        // Set 1 second per interval
        interval[uniquekey] = setInterval(() => {
          // Return if pause for spefici playlist is set to true
          if (pause[uniquekey] === true) return

          // Check counter every +10 seconds and update track data in db
          if (
            seconds % 10 === 0 &&
            seconds <= track.duration &&
            skip[uniquekey] === false
          ) {
            trackModel.update(conn, trackId, { position: seconds })
          }

          // Check if counter exceed track max duration or playlist is skip
          if (seconds > track.duration || skip[uniquekey] === true) {
            skip[uniquekey] = false
            index++
            seconds = 0
            clearInterval(interval[uniquekey])
            resolve()
          }

          // IO Emit to be used by player app
          io.emit('counter', {
            uid: playlist.uid,
            currentTrack: track.title,
            counter: seconds,
          })

          seconds++
        }, 1000)
      })
    }

    res.sendStatus(200)
  },

  pause: (req, res) => {

    const uniqueKey = `${req.params.trackId}_${req.user}`

    pause[uniqueKey] = !pause[uniqueKey]

    console.log(pause, 'pause')

    res.json({ pause: pause[uniqueKey] })
  },

  skip: (req, res) => {
    const uniqueKey = `${req.params.trackId}_${req.user}`

    skip[uniqueKey] = true

    res.sendStatus(200)
  },
}
