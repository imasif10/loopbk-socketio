const fetch = require('node-fetch')

const { TWOFA_URL } = require('../config')
const userModel = require('../database/models')('users')

module.exports = {
  login: (req, res) => {
    res.render('pages/auths/login.auth.ejs')
  },

  loginPost: async (req, res) => {
    const { email, password } = req.body

    await fetch(`${TWOFA_URL}/api/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password,
      //  token,
      }),
    })
      .then((response) => response.json())
      .then(async (response) => {
        if (response.error) {
          req.flash('error', response.error)
          return res.redirect('/login')
        }

        await req.login(response.user, (err) => {
          res.redirect('/')
        })
      })
  },

  logout: (req, res) => {
    req.logout()
    res.redirect('/login')
  },

  register: (req, res) => {
    res.render('pages/auths/register.auth.ejs')
  },

  registerPost: async (req, res) => {
    try {
      const conn = req.app.locals.conn
      const { name, email, password } = req.body
      const users = await userModel.get(conn, { email })

      if (users.length > 0) {
        throw 'User already exist!'
      }

      const response = await fetch(`${TWOFA_URL}/api/register`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          name,
          email,
          password,
        }),
      }).then((res) => res.json())
        res.redirect('/login')
    /*   res.render('pages/auths/qrCode.auth.ejs', {
        qrCodeUrl: response.qrCodeUrl,
        secret: response.secret,
      }) */
    } catch (error) {
      console.log(error)
      req.flash('error', error)
      res.redirect('/register')
    }
  },
}
