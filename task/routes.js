const passport = require('passport')

const authController = require('./controllers/authController')
const homeController = require('./controllers/homeController')
const trackController = require('./controllers/trackController')

module.exports = (app) => {
  app.get('/', checkAuthenticated, homeController.index)

  app.get('/tracks/:trackId', checkAuthenticated, trackController.view)
  app.post('/tracks', trackController.create)
  app.get('/tracks/:trackId/pause', trackController.pause)
  app.get('/tracks/:trackId/skip', trackController.skip)

  app.get('/login', checkNotAuthenticated, authController.login)
  app.post('/login', checkNotAuthenticated, authController.loginPost)
  app.post('/logout', authController.logout)
  app.get('/register', authController.register)
  app.post('/register', authController.registerPost)
}

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/login')
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/')
  }

  next()
}
